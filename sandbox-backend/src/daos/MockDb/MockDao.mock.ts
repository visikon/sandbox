import jsonfile from 'jsonfile';
import { Image } from '@daos/ImageDao';

interface MockDb {
    images: Image[];
}

export class MockDaoMock {

    private readonly dbFilePath = 'src/daos/MockDb/MockDb.json';

    protected openDb(): Promise<MockDb> {
        return jsonfile.readFile(this.dbFilePath);
    }

    protected saveDb(db: any): Promise<any> {
        return jsonfile.writeFile(this.dbFilePath, db);
    }
}
